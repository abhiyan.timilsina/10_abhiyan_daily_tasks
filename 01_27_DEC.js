/*
*Name:findConnectedComponents.js
*Description:Given a graph find the connected components in that graph
*Input:An adjacency matrix showing the graph
*Output:The number of components in the graph
*author:abhiyan timilsina
*/

//Accesing the graph class
const Graph = require('./Graph.js');

//Instantiating a new graph component
let graph = new Graph(12,0)

console.log(graph.adjacencyMatrix)

graph.addToGraph(0,1)
graph.addToGraph(0,2)
graph.addToGraph(1,2)
graph.addToGraph(2,3)
graph.addToGraph(4,5)
graph.addToGraph(4,6)
graph.addToGraph(4,7)
graph.addToGraph(5,6)  
graph.addToGraph(7,8)
graph.addToGraph(9,10)                                      

let connectedComponentsCount = 0;
let visited = [];
let i = 0
let count = 0;
while(1){
  if(visited.length==graph.verticesCount)
   break;
   connected_nodes = graph.dfsTraversal(i);
   visited = [...visited,...connected_nodes];
   count++;
   i=connected_nodes[connected_nodes.length-1]+1;
}
console.log(count);

                                            